public class Main {
    public static void main(String[] args) {
        Car car = new Car();
        car.TurnOnEngine();
        System.out.println( "Car isRunning " + car.GetState());
        car.TurnOffEngine();
        System.out.println("Car isRunning " + car.GetState());
    }
}
